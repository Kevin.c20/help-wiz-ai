import React from "react";
import "../App.css";
import { logo } from "../assets";


const Hero = () => {
  return (
    <header className='w-full flex justify-center items-center flex-col'>
      <nav className='flex justify-between items-center w-full mb-10 pt-3'>
        <img src={logo} alt='wiz_logo' className='w-28 object-contain' />

        <button
          type='button'
          onClick={() =>
            window.open("https://gitlab.com/Kevin.c20/help-wiz-ai", "_blank")
          }
          className='black_btn'
        >
          GitLab
        </button>
      </nav>
      <div className="wizzy"></div>
      <h1 className='head_text'>
        Summarize Articles with <br className='max-md:hidden' />
        <span className='purple_gradient'>Help Wiz AI</span>
      </h1>
      <h2 className='desc'>
        Enter a URL and summarize your reading with OpenAI, an open-source article summarizer
        that turns lengthy articles into clear and concise summaries.
      </h2>
    </header>
  );
};

export default Hero;