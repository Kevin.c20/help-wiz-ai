# Welcome To Help Wiz AI

**Help Wiz AI is an open source article summarizer that is intended to make readings shorter and your life easier!**

**Simply copy and paste an article URL of your choice and click the arrow. You will get a short and concise summary of your article. You're Welcome!**
